station-repacking
==============================

# Introduction
This repository contains all code developed in the process of writing my
master thesis at the Technical University of Munich. The thesis is titled
**The Complexity of Station Repacking**. 

# Summary of thesis
Station Repacking is an NP-complete problem that arose as an essential part of the FCC Incentive Auction. The Incentive Auction was the first auction designed to reallocate spectrum between two different uses: broadcast television and mobile data transmission. An instance of Station Repacking is given by a set of stations, a set of channels, a set of domain constraints and a set of interference constraints. The task is to decide whether there exists an assignment of channels to stations, which respects the interference constraints and the domain constraints. The success of the auction hinged upon the ability to solve hundreds of thousands of instances of Station Repacking. It was initially unclear whether this would be possible. After several years of investigations a solver, SATFC, which managed to do so, was developed. The development included the usage of a novel, automated parameter tuning techniques, as well as problem-specific insights.

It is unclear why SATFC succeeded, whereas other approaches did not. In this thesis, we try to understand why. Our hypothesis is that the instances encountered in the Incentive Auction were, in some sense, easy to solve, due to some hidden structure that the SATFC solver was able to take advantage of. We approach the question from two sides: On the theoretical side, we study the relationship between Station Repacking and several graph problems. We show that Station Repacking is a special case Independent Set, by finding a reduction. We survey known complexity results for different graph coloring problems, focusing on graph classes with nice properties with respect to coloring problems, and discuss their implications for Station Repacking. We also develop two pre-processing rules that can be applied to instances of Station Repacking, with the aim of uncovering the difficult parts of an instance. On the empirical side, we analyze a set of $10000$ instances, computing several properties for each instance to try and uncover structural properties which could potentially explain the success of the SATFC solver. Amongst the properties we compute, is the effect of the pre-processing rules developed in our theoretical approach.

The results obtained from the empirical study weakens the support for our hypothesis, and we are not able to provide any does explanation of the performce SATFC. Nevertheless, we have obtained a better understanding of the complexity of Station Repacking and of its relationship to other NP-complete problems.

# Data
Data and description of data is available upon request.

# Organization
------------

    ├── LICENSE
    ├── README.md 
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── notebooks          <- Jupyter notebooks.
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.testrun.org


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
