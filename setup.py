from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Analysis of the station repacking instances that arose in the FCC Incentive Auction',
    author='Haakon H.R.',
    license='MIT',
)
