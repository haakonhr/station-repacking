#These functions take the graph representing the interference model
#as input and returns different properties.
import networkx as nx
import math

import kernelization as krnl

from basic_functions import basic_functions as bf

print(dir(bf))

def createGraph(newStation, stationSet, interferenceConstraints):

    if newStation not in stationSet:
        print("New station not in kernel!")
        return nx.Graph()

    coCriteria = interferenceConstraints["interferenceType"] == "CO"
    stationCriteria = interferenceConstraints["targetID"].isin(stationSet)
    interferenceConstraints = interferenceConstraints[coCriteria & stationCriteria]
    #interferenceConstraints["interferingIDs"] = interferenceConstraints["interferingIDs"].apply(bf._intersectLists, list2=stationSet)

    E = []
    for station in stationSet:
        interferingStations = krnl._getInterferingStationsFast(station, stationSet,
            interferenceConstraints)
        for interferingStation in interferingStations:
            E.append((station, interferingStation))

    G = nx.Graph()
    G.add_nodes_from(stationSet)
    G.add_edges_from(E)

    # Get the component that contains the newStation
    #print("Number of connected components: ", len(nx.connected_components(G)))

    # If newStation is not in the kernel (can that happen)

    for cc in nx.connected_components(G):
        if newStation in cc:
            nodes = cc
            break

    Ecomp = []
    for e in E:
        if e[0] in nodes and e[1] in nodes:
            Ecomp.append(e)

    Gcomp = nx.Graph()
    Gcomp.add_nodes_from(nodes)
    Gcomp.add_edges_from(Ecomp)

    #print("Number of connected components 2: ", len(nx.connected_components(Gcomp)))

    return Gcomp

def degStats(G):
    avg = 2*len(G.edges)/len(G.nodes)
    hist = nx.degree_histogram(G)
    maxDeg = len(hist) - 1
    degreeSequence = [i for i in range(len(hist)) for j in range(hist[i])]
    length = len(degreeSequence)
    if len(degreeSequence) != len(G.nodes):
        print("Degree sequence does not have the correct length")
        return None
    
    if length % 2 == 0:
        median = (degreeSequence[int(length/2)] + degreeSequence[int((length/2)+1)])/2
    else:
        median = (degreeSequence[int(math.ceil(length/2))])

    return avg, maxDeg, median


def avgInterference(G):
    return 2*len(G.edges)/len(G.nodes)

def medianNumberOfInterference(G):
    degrees = nx.degree_histogram(G)
    degreeSequence = [i for i in range(len(degrees)) for j in range(degrees[i])]
    if len(degreeSequence) != len(G.nodes):
        print("Degree sequence does not have the correct length")
    length = len(degreeSequence)
    if length % 2 == 0:
        return (degreeSequence[int(length/2)] + degreeSequence[int((length/2)+1)])/2
    else:
        return (degreeSequence[int(math.ceil(length/2))])

def maxInterference(G):
    return len(nx.degree_histogram(G))-1

def avgDegreeCentrality(G):
	nxDegreeCentrality
	return (sum())

#Takes G and returns its spectrum as a numpy vector.
#The interesting eigenvalues are the second smallest and the largest.
def laplacianSpectrum(G):
	spectrum = nx.laplacian_spectrum(G)
	return spectrum[1], spectrum[len(spectrum)-1]

# Not used as the size is the same
# def size(G):
# 	return len(G.nodes)

def density(G):
	return ((2*len(G.edges))/(len(G.nodes)*(len(G.nodes)-1)))

def diameter(G):
    return nx.diameter(G)

def maximalClique(G, newStation):
    clqs = nx.node_clique_number(G, nodes=[newStation])
    return clqs[newStation]

#returns a list of size, avgdegree, maxdegree and diameter
# def informationAboutLargestComponent(G):
# 	largest_cc = max(nx.connected_components(G), key=len)
# 	Gtemp = nx.Graph()
# 	Gtemp.add_nodes_from(largest_cc)

# 	Etemp = []
# 	for e in G.edges():
# 		if (e[0] in largest_cc and e[1] in largest_cc):
# 			Etemp.append(e)
        
# 	Gtemp.add_edges_from(Etemp)	
	
# 	return [len(largest_cc), avgDegree(Gtemp), maxDegree(Gtemp), nx.diameter(Gtemp)]

