import multiprocessing
from itertools import product
from itertools import combinations

def merge_names2(a, b, c, d, e):
    return '{} & {}'.format(a, b, c, d, e)

def merge_names(a, b):
    return '{} & {}'.format(a, b)

if __name__ == '__main__':
    names = ['Brown', 'Wilson', 'Bartlett', 'Rivera', 'Molloy', 'Opie']
    p = multiprocessing.Pool(2)
    results = p.starmap(merge_names, combinations(names, repeat=2))
    # with multiprocessing.Pool(processes=3) as pool:
    #     results = pool.starmap(merge_names, product(names, repeat=2))
    print(results)