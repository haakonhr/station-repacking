def _intersectLists(list1, list2):
    intersectedList = [int(e) for e in list(set(list1).intersection(set(list2)))]
    intersectedList.sort()
    intersectedList = [str(e) for e in intersectedList]
    return intersectedList

def listify(stringOfIDs):
	lst = stringOfIDs[1:-1]
	lst = lst.split(", ")
	lst = [l[1:-1] for l in lst]
	return lst
