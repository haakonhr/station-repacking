# Import libraries
import pandas as pd
import networkx as nx
import random
import numpy as np
import time

from multiprocessing import Pool

# Import scripts
import compute_sp_properties as csp
import compute_graph_properties as cgp
import finding_relevant_component as frc
import kernelization as krnl

# Import custom functions and global parameters
from parameters import parameters
from basic_functions import basic_functions

print(parameters.minChannel)

#Set local names for functions
listify = basic_functions.listify
_intersectLists = basic_functions._intersectLists

pathToInterferenceConstraints = "/Users/haakonhr/station-repacking/data/interim/interferenceConstraints_pruned.csv"
myColNames = ["interferenceType", "targetChannel", "interferingChannel", "targetID", "interferingIDs"]
interferenceConstraints = pd.read_csv(pathToInterferenceConstraints,
                          names=myColNames)
interferenceConstraints["interferingIDs"] = interferenceConstraints["interferingIDs"].apply(listify)
interferenceConstraints["targetID"] = interferenceConstraints["targetID"].apply(str)
print(interferenceConstraints.head(5))

print(len(interferenceConstraints["targetID"].values))

def createSuperGraph(interferenceConstraints):

    stationSet = set(interferenceConstraints["targetID"].values)

    coCriteria = interferenceConstraints["interferenceType"] == "CO"
    stationCriteria = interferenceConstraints["targetID"].isin(stationSet)
    interferenceConstraints = interferenceConstraints[coCriteria & stationCriteria]
    #interferenceConstraints["interferingIDs"] = interferenceConstraints["interferingIDs"].apply(bf._intersectLists, list2=stationSet)

    E = []
    for station in stationSet:
        interferingStations = krnl._getInterferingStationsFast(station, stationSet,
            interferenceConstraints)
        for interferingStation in interferingStations:
            E.append((station, interferingStation))

    G = nx.Graph()
    G.add_nodes_from(stationSet)
    G.add_edges_from(E)

    # Get the component that contains the newStation
    #print("Number of connected components: ", len(nx.connected_components(G)))

    # If newStation is not in the kernel (can that happen)

    for cc in nx.connected_components(G):
        if newStation in cc:
            nodes = cc
            break

    Ecomp = []
    for e in E:
        if e[0] in nodes and e[1] in nodes:
            Ecomp.append(e)

    Gcomp = nx.Graph()
    Gcomp.add_nodes_from(nodes)
    Gcomp.add_edges_from(Ecomp)

    #print("Number of connected components 2: ", len(nx.connected_components(Gcomp)))

    return Gcomp

