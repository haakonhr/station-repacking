# Import libraries
import pandas as pd
import networkx as nx
import random
import numpy as np
import time

from multiprocessing import Pool

# Import scripts
import compute_sp_properties as csp
import compute_graph_properties as cgp
import finding_relevant_component as frc
import kernelization as krnl

# Import custom functions and global parameters
from parameters import parameters
from basic_functions import basic_functions

print(parameters.minChannel)

#Set local names for functions
listify = basic_functions.listify
_intersectLists = basic_functions._intersectLists

#### 1. IMPORT DATA ####

#Import pruned domains
pathToDomains = "/Users/haakonhr/station-repacking/data/interim/domains_pruned.csv"
myColNames = ["targetID", "domain"]
domains = pd.read_csv(pathToDomains, names=myColNames)
domains["domain"] = domains["domain"].apply(listify)
domains["targetID"] = domains["targetID"].apply(str)
domains.set_index("targetID", inplace = True)
print(domains.head())

#Import pruned interference constraints
pathToInterferenceConstraints = "/Users/haakonhr/station-repacking/data/interim/interferenceConstraints_pruned.csv"
myColNames = ["interferenceType", "targetChannel", "interferingChannel", "targetID", "interferingIDs"]
interferenceConstraints = pd.read_csv(pathToInterferenceConstraints,
                          names=myColNames)
interferenceConstraints["interferingIDs"] = interferenceConstraints["interferingIDs"].apply(listify)
interferenceConstraints["targetID"] = interferenceConstraints["targetID"].apply(str)
print(interferenceConstraints.head())

#Import instances
pathToInstances = "/Users/haakonhr/station-repacking/data/raw/extra_cacm_problem_info.csv"
instances = pd.read_csv(pathToInstances)
instances = instances.rename(columns={"stations":"stationList", "name":"id"})
instances.set_index("id", inplace=True, drop=True)
print(instances.head(5))

# 2. COMPUTE PROPERTIES FOR ALL SP AND THEIR CORRESPONDING G^D 
# (Kernel only or for both??)

kernelResults = {}
componentResults = {}

#### TESTING ####

#instanceLocs = np.random.choice(len(instances), 100, replace=False)

instanceIDs1 = instances.iloc[:1000]
instanceIDs1 = instanceIDs1.index.values.tolist()
instanceIDs2 = instances.iloc[1000:2000].index.values.tolist()
instanceIDs3 = instances.iloc[2000:3000].index.values.tolist()
instanceIDs4 = instances.iloc[3000:4000].index.values.tolist()
instanceIDs5 = instances.iloc[4000:5000].index.values.tolist()
instanceIDs6 = instances.iloc[5000:6000].index.values.tolist()
instanceIDs7 = instances.iloc[6000:7000].index.values.tolist()
instanceIDs8 = instances.iloc[7000:8000].index.values.tolist()
instanceIDs9 = instances.iloc[8000:9000].index.values.tolist()
instanceIDs10 = instances.iloc[9000:10000].index.values.tolist()

def computeProperties(newStation, stationSet, interferenceConstraints = interferenceConstraints,
    domains = domains):
    #start = time.time()
    #print("Computing properties..")
    sz = csp.getSize(stationSet)
    ratio = csp.getRatio(stationSet, interferenceConstraints, domains)
    print("Ratio: ", ratio)
    avgDomain = csp.avgDomainSize(stationSet, domains)
    szDomain = csp.getInfoNewStation(newStation, domains)
    results = [sz, ratio, avgDomain, szDomain]
    #end = time.time()
    #print("Computing properties take ", end-start, " seconds")
    return results

# Compute graph properties for the intersection graph corresponding
# to an SP instance
def graphProperties(G, newStation):
    sz = len(G.nodes)
    if newStation not in G.nodes:
        maxClq = 1
    else:
        maxClq = cgp.maximalClique(G, newStation)
    density = cgp.density(G)
    diameter = cgp.diameter(G)
    lambda2, lambdaN = cgp.laplacianSpectrum(G)
    avgDeg, maxDeg, medDeg = cgp.degStats(G)
    results = [sz, avgDeg, maxDeg, medDeg,
        density, diameter, lambda2, lambdaN, maxClq]

    return results

def pipeline(instanceID, interferenceConstraints = interferenceConstraints,
    domains = domains):
    try:
        print("InstanceID: ", instanceID)
        stationListComponent, newStation, szNeigh = frc.createComponentInclNewStation(instanceID, instances, interferenceConstraints)
        
        # We compute the average, max and min interference coefficient inside kernelize()
        kernelizationResults = krnl.kernelize(set(stationListComponent), interferenceConstraints, domains)
        kernel = kernelizationResults[0]

        # sz, ratio, avgDomain, szDomain for station
        componentResults = computeProperties(newStation, stationListComponent, interferenceConstraints,
            domains)

        # szDomain, szNeigh, maxClq (appended later)
        stResults = [componentResults[-1], szNeigh]
        componentResults.pop()

        if len(kernel) > 0 and newStation in kernel:
            kernelResults = computeProperties(newStation, kernel, interferenceConstraints,
                domains)
        else:
            kernelResults = [0 for i in range(len(componentResults))]
        kernelResults.pop()

        componentResults.append(kernelizationResults[2])
        kernelResults.append(kernelizationResults[1])

        #Gcomp = cgp.createGraph(stationListComponent, interferenceConstraints)
        # sz, ratio, avgDomain, szKernel, ratioKernel, avgDomainKernel
        SPresults = componentResults[:-1] + kernelResults[:-1]

        if len(kernel) > 0 and newStation in kernel:
            start = time.time()
            Gkern = cgp.createGraph(newStation, kernel, interferenceConstraints)
            graphResults = graphProperties(Gkern, newStation)
            stResults.append(graphResults[8])
            graphResults = graphResults[:8]
            end = time.time()
            print("Graph properties takes ", end-start, " seconds")
        else:
            print("New station not in kernel!")
            graphResults = [0 for i in range(9)]
            stResults.append(graphResults[8])
            graphResults = graphResults[:8]

        #The results are structured as follows:
        #size, ratio, avg i(s), avgDomain, szNeighborhood, domainNewStation, cliqueSize, 
        #density, avgDegree, medianDegree, maxDegree, diameter, eigenvalue2, eigenvalueLarge
        return [instanceID]+SPresults+graphResults+stResults
    except Exception:
        return [instanceID]+[0 for i in range(17)]

#3. COMPUTE WRITE OUT RESULTS
if __name__ == '__main__':
    print("Multiprocessing..")
    p = Pool(2)
    results1 = p.map(pipeline, instanceIDs1)
else:
    print("Not run directly..")

print(results1)
#results1 = np.array(results1)c

resultsTest = pd.DataFrame(results1, columns=["id", "szComp", "ratioComp",
    "avgDomainComp", "szKernel", "ratioComp", "avgDomainComp", "V", "avgDeg",
    "maxDeg", "medDeg", "density", "diameter", "lambda2",
    "lambdaN", "domainNew", "nghbNew", "clqNew"])

results1 = pd.DataFrame(results1, columns=["id", "szComp", "ratioComp",
    "avgDomainComp", "szKernel", "ratioComp", "avgDomainComp", "V", "avgDeg",
    "maxDeg", "medDeg", "density", "diameter", "lambda2",
    "lambdaN", "domainNew", "nghbNew", "clqNew"])
#results1.to_csv("results1.csv", header=True, index="id")

if __name__ == '__main__':
    print("Multiprocessing..")
    p = Pool(4)
    results2 = p.map(pipeline, instanceIDs2)
else:
    print("Not run directly..")

#results2 = np.array(results2)
print(results2)

results2 = pd.DataFrame(results2, columns=["id", "szComp", "ratioComp",
    "avgDomainComp", "szKernel", "ratioComp", "avgDomainComp", "V", "avgDeg",
    "maxDeg", "medDeg", "density", "diameter", "lambda2",
    "lambdaN", "domainNew", "nghbNew", "clqNew"])
results2.to_csv("results2.csv", header=True, index="id")

if __name__ == '__main__':
    print("Multiprocessing..")
    p = Pool(4)
    results3 = p.map(pipeline, instanceIDs3)
else:
    print("Not run directly..")

#results3 = np.array(results3)

results3 = pd.DataFrame(results3, columns=["id", "szComp", "ratioComp",
    "avgDomainComp", "szKernel", "ratioComp", "avgDomainComp", "V", "avgDeg",
    "maxDeg", "medDeg", "density", "diameter", "lambda2",
    "lambdaN", "domainNew", "nghbNew", "clqNew"])
results3.to_csv("results3.csv", header=True, index="id")

if __name__ == '__main__':
    print("Multiprocessing..")
    p = Pool(4)
    results4 = p.map(pipeline, instanceIDs4)
else:
    print("Not run directly..")

#results4 = np.array(results4)

results4 = pd.DataFrame(results4, columns=["id", "szComp", "ratioComp",
    "avgDomainComp", "szKernel", "ratioComp", "avgDomainComp", "V", "avgDeg",
    "maxDeg", "medDeg", "density", "diameter", "lambda2",
    "lambdaN", "domainNew", "nghbNew", "clqNew"])
results4.to_csv("results4.csv", header=True, index="id")

if __name__ == '__main__':
    print("Multiprocessing..")
    p = Pool(4)
    results5 = p.map(pipeline, instanceIDs5)
else:
    print("Not run directly..")

#results5 = np.array(results5)

results5 = pd.DataFrame(results5, columns=["id", "szComp", "ratioComp",
    "avgDomainComp", "szKernel", "ratioComp", "avgDomainComp", "V", "avgDeg",
    "maxDeg", "medDeg", "density", "diameter", "lambda2",
    "lambdaN", "domainNew", "nghbNew", "clqNew"])
results5.to_csv("results5.csv", header=True, index="id")

if __name__ == '__main__':
    print("Multiprocessing..")
    p = Pool(4)
    results6 = p.map(pipeline, instanceIDs6)
else:
    print("Not run directly..")

#results6 = np.array(results6)

results6 = pd.DataFrame(results6, columns=["id", "szComp", "ratioComp",
    "avgDomainComp", "szKernel", "ratioComp", "avgDomainComp", "V", "avgDeg",
    "maxDeg", "medDeg", "density", "diameter", "lambda2",
    "lambdaN", "domainNew", "nghbNew", "clqNew"])
results6.to_csv("results6.csv", header=True, index="id")

if __name__ == '__main__':
    print("Multiprocessing..")
    p = Pool(4)
    results7 = p.map(pipeline, instanceIDs7)
else:
    print("Not run directly..")

#results7 = np.array(results7)

results7 = pd.DataFrame(results7, columns=["id", "szComp", "ratioComp",
    "avgDomainComp", "szKernel", "ratioComp", "avgDomainComp", "V", "avgDeg",
    "maxDeg", "medDeg", "density", "diameter", "lambda2",
    "lambdaN", "domainNew", "nghbNew", "clqNew"])
results7.to_csv("results7.csv", header=True, index="id")

if __name__ == '__main__':
    print("Multiprocessing..")
    p = Pool(4)
    results8 = p.map(pipeline, instanceIDs8)
else:
    print("Not run directly..")

#results8 = np.array(results8)

results8 = pd.DataFrame(results8, columns=["id", "szComp", "ratioComp",
    "avgDomainComp", "szKernel", "ratioComp", "avgDomainComp", "V", "avgDeg",
    "maxDeg", "medDeg", "density", "diameter", "lambda2",
    "lambdaN", "domainNew", "nghbNew", "clqNew"])
results8.to_csv("results8.csv", header=True, index="id")

if __name__ == '__main__':
    print("Multiprocessing..")
    p = Pool(4)
    results9 = p.map(pipeline, instanceIDs9)
else:
    print("Not run directly..")

#results9 = np.array(results9)

results9 = pd.DataFrame(results9, columns=["id", "szComp", "ratioComp",
    "avgDomainComp", "szKernel", "ratioComp", "avgDomainComp", "V", "avgDeg",
    "maxDeg", "medDeg", "density", "diameter", "lambda2",
    "lambdaN", "domainNew", "nghbNew", "clqNew"])
results9.to_csv("results9.csv", header=True, index="id")

if __name__ == '__main__':
    print("Multiprocessing..")
    p = Pool(4)
    results10 = p.map(pipeline, instanceIDs10)
else:
    print("Not run directly..")

#results10 = np.array(results10)

results10 = pd.DataFrame(results10, columns=["id", "szComp", "ratioComp",
    "avgDomainComp", "szKernel", "ratioComp", "avgDomainComp", "V", "avgDeg",
    "maxDeg", "medDeg", "density", "diameter", "lambda2",
    "lambdaN", "domainNew", "nghbNew", "clqNew"])
results10.to_csv("results10.csv", header=True, index="id")

