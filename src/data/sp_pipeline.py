# Import libraries
import pandas as pd
import networkx as nx
import random
import numpy as np

from multiprocessing import Pool

# Import scripts
import compute_sp_properties as csp
import finding_relevant_component as frc
import kernelization as krnl


# Import custom functions and global parameters
from parameters import parameters
from basic_functions import basic_functions

print(parameters.minChannel)

#Set local names for functions
listify = basic_functions.listify
_intersectLists = basic_functions._intersectLists

#### 1. IMPORT DATA ####

#Import pruned domains
pathToDomains = "/Users/haakonhr/station-repacking/data/interim/domains_pruned.csv"
myColNames = ["targetID", "domain"]
domains = pd.read_csv(pathToDomains, names=myColNames)
domains["domain"] = domains["domain"].apply(listify)
domains["targetID"] = domains["targetID"].apply(str)
domains.set_index("targetID", inplace = True)
print(domains.head())

#Import pruned interference constraints
pathToInterferenceConstraints = "/Users/haakonhr/station-repacking/data/interim/interferenceConstraints_pruned.csv"
myColNames = ["interferenceType", "targetChannel", "interferingChannel", "targetID", "interferingIDs"]
interferenceConstraints = pd.read_csv(pathToInterferenceConstraints,
                          names=myColNames)
interferenceConstraints["interferingIDs"] = interferenceConstraints["interferingIDs"].apply(listify)
interferenceConstraints["targetID"] = interferenceConstraints["targetID"].apply(str)
print(interferenceConstraints.head())

#Import instances
pathToInstances = "/Users/haakonhr/station-repacking/data/raw/extra_cacm_problem_info.csv"

instances = pd.read_csv(pathToInstances)
instances = instances.rename(columns={"stations":"stationList", "name":"id"})
instances.set_index("id", inplace=True, drop=True)
print(instances.head(5))

# 2. COMPUTE PROPERTIES FOR ALL SP AND THEIR CORRESPONDING G^D 
# (Kernel only or for both??)

#### TESTING ####

instancesTemp1 = instances.iloc[:20]

def pipeline_test(instanceID, interferenceConstraints = interferenceConstraints,
    domains = domains):
    stationListComponent = frc.createComponentInclNewStation(instanceID, instances, interferenceConstraints)
    kernelizationResults = krnl.kernelize(set(stationListComponent), interferenceConstraints, domains)
    kernel = kernelizationResults[0]
    return (len(stationListComponent), kernelizationResults[2], len(kernel),
        kernelizationResults[1]) 
    
instanceIDs = instancesTemp1.index.values.tolist()
if __name__ == '__main__':
    print("Multiprocessing..")
    p = Pool(parameters.numOfProcesses)
    results = p.map(pipeline_test, instanceIDs2)
else:
    print("Not run directly..")

print(results)
def pipeline(instances, interferenceConstraints, domains):
    pass

# Compute properties for the SP formulation. Takes the relevant
# set of stations, interferenceConstriants
def _sp_properties(stationSet, interferenceConstraints, domains):
    pass

# Compute graph properties for the intersection graph corresponding
# to an SP instance
def _graph_properties(G, newStation):
    pass

counter = 0
for row in instances.itertuples():
    counter += 1
    instanceID = row.Index
    print("Instance: ", instanceID)

    stationList = finding_relevant_component._getStationList(instanceID, instances)
    print(len(stationList))
    #print(finding_relevant_component.getNeighborhood(stationList[0], stationList,
    #    interferenceConstraints))
    stationListComponent = finding_relevant_component.createComponentInclNewStation(instanceID,
         instances, interferenceConstraints)

    #print(len(stationList))
    print(len(stationListComponent))
    print(len(kernelization.kernelize(set(stationListComponent), interferenceConstraints, domains)))

    print(compute_sp_properties.avgDomainSize(stationListComponent, interferenceConstraints, domains))
    if counter > 20:
        break

# 3. WRITE OUT RESULTS


