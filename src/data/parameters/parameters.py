# This file contains global constants that are used throughout the project
# to only consider the relevant data. In the FCC Incentive Auction the channels
# available for re-packing, in the last iteration, were channels 14 to 36.

# The runtimeCutoff is a way to try and compare apples with apples, not oranges.
# In this case the oranges would be all the instances that were solved by during
# pre-processing by just looking at smaller subproblems that only consider the
# neighborhoods of some order of the last station added

#Channels have to be in the range 1-49
minChannel = 14
maxChannel = 36

# This is used to segment the data into easy and hard instances
# based on runtime. 0.862 is the runtime of the fastest
# UNSAT instance
runtimeCutoff = 0.862

# Number of cores to be used to run the pipeline
numOfCores = 3