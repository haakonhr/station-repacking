#This code detects the relevant connected component of an instance, i.e.
#the instance containing the last vertex added

from parameters import parameters
import time

#Takes a data frame of instances and an instanceID
#and returns a list of stations that have a previous
#assignment
def _getStationList(instanceID, instances):
    stationList = instances.loc[instanceID, "stationList"][1:-1]
    stationList = stationList.split(", ")
    return stationList
    
#Takes a data frame of instances and an instanceID
#and returns a dictionary of the assignment
def _getAssignmentDict(instanceID, instances):
    assignment = instances.loc[instanceID, "assignment"][1:-1]
    assignment = assignment.split(",")
    stations = [stationChannel.split(":")[0][1:-1] for stationChannel in assignment]
    channels = [stationChannel.split(":")[1] for stationChannel in assignment]
    assignment = dict(zip(stations, channels))
    return assignment

def _findNewStation(stationList, assignment):
    assignedStations = [k for k,v in assignment.items() 
                        if int(v) >= parameters.minChannel and int(v) <= parameters.maxChannel]
    newStations = list(set(stationList) - set(assignedStations))
    if len(newStations) > 1:
        print("There are ", len(newStations), 
            " unassigned stations in the instance, which is too many")
        return None

    return newStations[0]

def _intersectLists(list1, list2):
    intersectedList = [int(e) for e in list(set(list1).intersection(set(list2)))]
    intersectedList.sort()
    intersectedList = [str(e) for e in intersectedList]
    return intersectedList

def getNeighborhood(station, stationList, interferenceConstraints):
    stationCriteria = interferenceConstraints["targetID"] == station
    interferenceConstraints = interferenceConstraints[stationCriteria]
    interferingStations = set()

    for row in interferenceConstraints.itertuples():
        interferingStations.update(row.interferingIDs)

    interferingStations = interferingStations.intersection(set(stationList))
    #print(interferingStations)

    return interferingStations

def getNeighborhoodFast(station, stationList, interferenceConstraints):
    stationCriteria = interferenceConstraints["targetID"] == station
    interferenceConstraints = interferenceConstraints[stationCriteria]["interferingIDs"].values
    interferingStations = set()

    interferingStations = set([e for lst in interferenceConstraints for e in lst])
    
    # for lst in interferenceConstraints:
    #     interferingStations = interferingStations | set(lst)

    # if interferingStations != interferingStations2:
    #     print("FATAL!")

    interferingStations = interferingStations.intersection(set(stationList))
    #print(interferingStations)

    return interferingStations

#Takes instance id and the set of instances as input and returns the
#list of unassigned stations, which should consist of exactly one station               
def findNewStation(instanceID, instances):
    stationList = _getStationList(instanceID, instances)
    assignmentDict = _getAssignmentDict(instanceID, instances)
    newStation = _findNewStation(stationList, assignmentDict)
    
    return(newStation)

def createComponentInclNewStation(instanceID, instances, interferenceConstraints):
    #start = time.time()
    stationList = _getStationList(instanceID, instances)
    newStation = findNewStation(instanceID, instances)
    V = [newStation]
    szNeigh = len(getNeighborhoodFast(newStation, stationList, interferenceConstraints))
    visited = set()
    counter = 0
    #DFS search
    while V:
        counter += 1
        station = V.pop()
        if station not in visited:
            visited.add(station)
            neighborhood = getNeighborhoodFast(station, stationList, interferenceConstraints)
            neighborhood = neighborhood - visited
            neighborhood = list(neighborhood)
            V.extend(neighborhood)

    #end = time.time()
    #print("Creating component: ", end - start, " seconds")
    return visited, newStation, szNeigh
