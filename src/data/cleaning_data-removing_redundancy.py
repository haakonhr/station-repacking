# Import libraries and packages
import pandas as pd

# Import custom functions and global parameters
from parameters import parameters
from basic_functions import basic_functions

listify = basic_functions.listify
_intersectLists = basic_functions._intersectLists

pathToInterferenceConstraints = "/Users/haakonhr/station-repacking/data/raw/Interference_Paired.csv"
pathToDomains = "/Users/haakonhr/station-repacking/data/raw/Domain.csv"

#Read in interference constraints
myColNames = ["interferenceType", "targetChannel", "interferingChannel",
	"targetID", "interferingIDs"]
interferenceConstraints = pd.read_table(pathToInterferenceConstraints,
	header=None, names=["interferenceType"])

#Make the column of interferingIDs into a list
interferenceConstraints[myColNames] = interferenceConstraints.interferenceType.str.split(",", n=4, expand=True)
interferenceConstraints.interferingIDs = interferenceConstraints.interferingIDs.str.split(",")

#Remove the rows not concerning the relevant channels
interferenceConstraints[["targetChannel", "interferingChannel"]] = interferenceConstraints[["targetChannel", "interferingChannel"]].apply(pd.to_numeric)
interferenceConstraints = interferenceConstraints[(interferenceConstraints["targetChannel"].between(parameters.minChannel, parameters.maxChannel))
	& (interferenceConstraints["interferingChannel"].between(parameters.minChannel, parameters.maxChannel))]

#Read in domain constraints
myColNamesLists = ["type", "targetID", "domain"]

domains = pd.read_table(pathToDomains, header=None, names=["type"])
domains[myColNamesLists] = domains.type.str.split(",", n=2, expand=True)
domains.domain = domains.domain.str.split(",")

domains = domains.drop("type",1)
domains = domains.set_index("targetID", drop=True)

# def _intersectLists(list1, list2):
#     intersectedList = [int(e) for e in list(set(list1).intersection(set(list2)))]
#     intersectedList.sort()
#     intersectedList = [str(e) for e in intersectedList]
#     return intersectedList
    
domains["domain"] = domains["domain"].apply(_intersectLists, 
	list2 = [str(i) for i in range(parameters.minChannel, parameters.maxChannel+1)])

#This block removes all redundant constraints where the target channel is not in the domain
#of the target station (targetID).
for constraint in interferenceConstraints.itertuples():
    if str(constraint.targetChannel) not in domains.loc[constraint.targetID, "domain"]:
        print(constraint.targetChannel, " is not in the domain of ", constraint.targetID
              , ": ", domains.loc[constraint.targetID, "domain"])
        interferenceConstraints.drop(constraint.Index, inplace=True)

print("Out of loop")
#Remove constraints concerning stations that are trivially unrestricted, in the sense
#that they have a channel in their domain without any interference
unconstrainedCriteria = interferenceConstraints["interferingIDs"].isnull() == True
unconstrainedStations = interferenceConstraints[unconstrainedCriteria]["targetID"]

stationConstrained = interferenceConstraints["targetID"].isin(unconstrainedStations) == False
interferenceConstraints = interferenceConstraints[stationConstrained]

#Remove redundancy from the domains file.
nonEmptyDomains = domains["domain"].apply(len) > 0
unconstrainedStationsSet = set(unconstrainedStations)
constrainedStations_domain = domains.index.isin(unconstrainedStationsSet) == False

#domains = domains[nonEmptyDomains & constrainedStations_domain]
domains = domains[nonEmptyDomains]

#Write to file
# domains.to_csv("/Users/haakonhr/station-repacking/data/interim/domains_pruned.csv",
# 	index=True, header=False)

# interferenceConstraints.to_csv(
# 	"/Users/haakonhr/station-repacking/data/interim/interferenceConstraints_pruned.csv",
# 	header=False, index=False)