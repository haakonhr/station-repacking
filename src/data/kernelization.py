#This file contains the functions used to kernelize an instance of the
#station repacking problem

import time

#Given a stationID, a set of stations and interference constraints
#the set of interfering stations, intersected with the set of stations 
#given as input, is returned.
def _getInterferingStations(stationID, stationSet, interferenceConstraints):
    stationID = str(stationID)
    interferingStationsSet = set()
    interferenceConstraints = interferenceConstraints.loc[interferenceConstraints["targetID"] == stationID]
    for row in interferenceConstraints.itertuples():
        interferingStationsSet.update(row.interferingIDs)

    interferingStationsSet = interferingStationsSet.intersection(set(stationSet))

    return interferingStationsSet 

def _getInterferingStationsFast(stationID, stationSet, interferenceConstraints):
    stationID = str(stationID)
    interferingStations = set()
    stationCriteria = interferenceConstraints["targetID"] == stationID
    interferenceConstraints = interferenceConstraints.loc[stationCriteria, "interferingIDs"]
    interferenceConstraints = interferenceConstraints.dropna()
    interferingStations = set([e for lst in interferenceConstraints for e in lst])
    # for lst in interferenceConstraints:
    #     interferingStationsSet = interferingStationsSet | set(lst)

    interferingStations = interferingStations.intersection(set(stationSet))

    return interferingStations

#Version two of the function above which also takes the interference type
#into account
def _getTypeInterferingStations(stationID, stationSet, interferenceType, interferenceConstraints):
    interferingStationsSet = set()
    criteriaID = interferenceConstraints["targetID"] == stationID
    criteriaType = interferenceConstraints["interferenceType"] == interferenceType
    interferenceConstraints = interferenceConstraints[criteriaType & criteriaID]
    for row in interferenceConstraints.itertuples():
        interferingStationsSet.update(row.interferingIDs)

    interferingStationsSet = interferingStationsSet.intersection(set(stationSet))
    
    return interferingStationsSet

def _getTypeInterferingStationsFast(stationID, stationSet, interferenceType, interferenceConstraints):
    interferingStations = set()
    criteriaID = interferenceConstraints["targetID"] == stationID
    criteriaType = interferenceConstraints["interferenceType"] == interferenceType
    interferenceConstraints = interferenceConstraints[criteriaType & criteriaID]["interferingIDs"].values
    
    interferingStations = set([e for lst in interferenceConstraints for e in lst])
    # for lst in interferenceConstraints:
    #     interferingStations = interferingStations | set(lst)

    interferingStations = interferingStations.intersection(set(stationSet))
    
    return interferingStations

#Takes a station, a station set and interference constraints as input
#and returns the interference coefficient, w.r.t. to the station set given
#as input.
def interferenceCoefficient(stationID, stationSet, interferenceConstraints):
    interferenceWeights = {}
    interferenceConstraints = interferenceConstraints.loc[interferenceConstraints["targetID"] == stationID]
    interferingStationsSet = _getInterferingStations(stationID, stationSet, interferenceConstraints)
    for interferingStation in interferingStationsSet:
        interferenceWeights[interferingStation] = 1
    if _getTypeInterferingStations(stationID,stationSet,"ADJ-2", interferenceConstraints):
        for interferingStation in _getTypeInterferingStations(stationID,stationSet,"ADJ-2", interferenceConstraints):
            interferenceWeights[interferingStation] += 1
    if _getTypeInterferingStations(stationID,stationSet,"ADJ-1", interferenceConstraints):
        for interferingStation in _getTypeInterferingStations(stationID,stationSet,"ADJ-1", interferenceConstraints):
            interferenceWeights[interferingStation] += 1
    if _getTypeInterferingStations(stationID,stationSet,"ADJ+1", interferenceConstraints):
        for interferingStation in _getTypeInterferingStations(stationID,stationSet,"ADJ+1", interferenceConstraints):
            interferenceWeights[interferingStation] += 1
    if _getTypeInterferingStations(stationID,stationSet,"ADJ+2", interferenceConstraints):
        for interferingStation in _getTypeInterferingStations(stationID,stationSet,"ADJ+2", interferenceConstraints):
            interferenceWeights[interferingStation] += 1
            
    return sum(interferenceWeights.values())

def interferenceCoefficientFast(stationID, stationSet, interferenceConstraints):
    interferenceWeights = {}
    interferenceConstraints = interferenceConstraints.loc[interferenceConstraints["targetID"] == stationID]
    interferingStationsSet = _getInterferingStationsFast(stationID, stationSet, interferenceConstraints)
    for interferingStation in interferingStationsSet:
        interferenceWeights[interferingStation] = 1
    if _getTypeInterferingStations(stationID,stationSet,"ADJ-2", interferenceConstraints):
        for interferingStation in _getTypeInterferingStationsFast(stationID,stationSet,"ADJ-2", interferenceConstraints):
            interferenceWeights[interferingStation] += 1
    if _getTypeInterferingStations(stationID,stationSet,"ADJ-1", interferenceConstraints):
        for interferingStation in _getTypeInterferingStationsFast(stationID,stationSet,"ADJ-1", interferenceConstraints):
            interferenceWeights[interferingStation] += 1
    if _getTypeInterferingStations(stationID,stationSet,"ADJ+1", interferenceConstraints):
        for interferingStation in _getTypeInterferingStationsFast(stationID,stationSet,"ADJ+1", interferenceConstraints):
            interferenceWeights[interferingStation] += 1
    if _getTypeInterferingStations(stationID,stationSet,"ADJ+2", interferenceConstraints):
        for interferingStation in _getTypeInterferingStationsFast(stationID,stationSet,"ADJ+2", interferenceConstraints):
            interferenceWeights[interferingStation] += 1
            
    return sum(interferenceWeights.values())    

# This function kernelizes using theorem ??. The average interference
# coefficient for both kernel and component is also returned.
def kernelize(stationSet, interferenceConstraints, domains):
    #start = time.time()
    stationSet = set(stationSet)  # ensure correct type
    counter = 0
    numOfStations = len(stationSet)
    U = set([0])
    interferenceCoeffsKernel = 0
    interferenceCoeffsComponent = 0
    maxCoeffKernel = 0
    maxCoeffComponent = 0
    while U:
        U = set()
        counter += 1
        if counter > 1:
            interferenceCoeffsKernel = 0
        #print("Iteration number: ", counter)
        for station in stationSet:
            interferenceCoefficientTemp = interferenceCoefficientFast(station, stationSet, interferenceConstraints)
            if counter == 1:
                interferenceCoeffsComponent += interferenceCoefficientTemp
                interferenceCoeffsKernel = interferenceCoeffsComponent
                if interferenceCoefficientTemp > maxCoeffComponent:
                    maxCoeffComponent = interferenceCoefficientTemp

            else:
                interferenceCoeffsKernel += interferenceCoefficientTemp

            if interferenceCoefficientTemp < len(domains.loc[int(station), "domain"]):
                U.add(station)
            
        stationSet = stationSet - U

    kernelSize = len(stationSet)

    if kernelSize > 0:
        avgInterferenceCoeffsKernel = interferenceCoeffsKernel / len(stationSet)
    else:
        avgInterferenceCoeffsKernel = 0

    avgInterferenceCoeffsComp = interferenceCoeffsComponent / numOfStations

    #end = time.time()
    #print("Kernelization takes ", end-start, " seconds")
        
    return stationSet, avgInterferenceCoeffsKernel, avgInterferenceCoeffsComp
