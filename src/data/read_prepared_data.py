import pandas as pd
#from .. import basic_functions as bf
#import Users.haakonhr.station-repacking.src.basic_functions as bf

#bf = importlib.import_module(".Users.haakonhr.station-repacking.src.basic_functions")

def listify(stringOfIDs):
    lst = stringOfIDs[1:-1]
    lst = lst.split(", ")
    lst = [l[1:-1] for l in lst]
    return lst

# l1 = [1,2,3]
# l2 = [3,4,5]
# print(bf._intersectLists(l1, l2))
pathToDomains = "/Users/haakonhr/station-repacking/data/interim/domains_pruned.csv"
pathToInterferenceConstraints = "/Users/haakonhr/station-repacking/data/interim/interferenceConstraints_pruned.csv"

#This code block reads in the pruned interference constraints
myColNames = ["interferenceType", "targetChannel", "interferingChannel", "targetID", "interferingIDs"]
interferenceConstraints = pd.read_csv(pathToInterferenceConstraints,
                          names=myColNames)

interferenceConstraints["interferingIDs"] = interferenceConstraints["interferingIDs"].apply(listify)
interferenceConstraints["targetID"] = interferenceConstraints["targetID"].apply(str)

#This code block reads in the pruned domains
myColNames = ["targetID", "domain"]
domains = pd.read_csv(pathToDomains, names=myColNames)

domains["domain"] = domains["domain"].apply(listify)

#This code block reads in the instances
pathToInstances = "/Users/haakonhr/station-repacking/data/raw/extra_cacm_problem_info.csv"

instances = pd.read_csv(pathToInstances)
instances = instances.rename(columns={"stations":"stationList", "name":"id"})
instances.set_index("id", inplace=True, drop=True)