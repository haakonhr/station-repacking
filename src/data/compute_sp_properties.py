#This script contains functions to compute properties of an
#SP instance given as a set of stations, interference constraints
#and domains

import finding_relevant_component as frc

def getSize(stationSet):
    return len(stationSet)

def getRatio(stationSet, interferenceConstraints, domains):
    numOfVars = sum(domains[domains.index.isin(stationSet)]["domain"].apply(len))
    stationCriteria = interferenceConstraints["targetID"].isin(stationSet)
    numOfIntClauses = (sum(interferenceConstraints[stationCriteria]["interferingIDs"].apply(len)))/2
    numOfDomClauses = len(stationSet)
    numOfMaxOneClauses = 0 #Not necessary, as assigning several channels to a station is fine
    
    if numOfVars > 0:
        return (numOfIntClauses + numOfDomClauses)/numOfVars
    else:
        return 0

# Returned in getRation instead
def getNumberOfInterferenceConstraints(stationSet, interferenceConstraints, domains):
    stationCriteria = interferenceConstraints["targetID"].isin(stationSet)
    lens = interferenceConstraints[stationCriteria]["interferingIDs"].apply(len)
    return lens/2

# REMEMBER THAT THE INTCOEFF IS COMPUTED DURING KERNELIATION. SAVE IT!
def interferenceCoeffStats(stationSet, interferenceConstraints, domains):
    pass

# Computed in kernelization function
def _avgInterferenceCoeff(stationSet, interferenceConstraints, domains):
    return 0

# Can be computed later from number of stations and average domain size
def sizeStableSetInstance(stationSet, domains, interferenceConstraints):
    return 0

# Will not be used, but is it interesting?
def _medianInterferenceCoeff(stationSet, interferenceConstraints, domains):
    return 0

def avgDomainSize(stationSet, domains):
    numberOfStations = len(stationSet)
    domains = domains[domains.index.isin(stationSet)]
    sumOfDomains = sum(domains["domain"].apply(len))
    return sumOfDomains/numberOfStations

# Gets the interference statistics of the new station, such as interference
# coefficient, degree und so weiter..
def getInfoNewStation(newStation, domains):
    #szNghbhood = len(frc.getNeighborhood(newStation, stationSet, interferenceConstraints))
    szDomain = len(domains[domains.index == newStation]["domain"][0])
    # UGLY QUICK FIX
    return szDomain



