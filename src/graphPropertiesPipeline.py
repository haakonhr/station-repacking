import networkx as nx
import pandas as pd
import random
import numpy as np
import matplotlib.pyplot as plt

def avgDegree(G):
    return 2*len(G.edges)/len(G.nodes)

def maxDegree(G):
    return len(nx.degree_histogram(G))-1

#returns a list of size, avgdegree, maxdegree and diameter
def informationAboutLargestComponent(G):
	largest_cc = max(nx.connected_components(G), key=len)
	Gtemp = nx.Graph()
	Gtemp.add_nodes_from(largest_cc)

	Etemp = []
	for e in G.edges():
		if (e[0] in largest_cc and e[1] in largest_cc):
			Etemp.append(e)
        
	Gtemp.add_edges_from(Etemp)	
	
	return [len(largest_cc), avgDegree(Gtemp), maxDegree(Gtemp), nx.diameter(Gtemp)]

#This function returns the largest k such that G is P_k free
def Pkfree(G):
	return 0

def girth(G):
	return 0

#Here we read in the data
pathToData = "/Users/haakonhr/Dropbox/TUM/Masteroppgave/FCC/"

dfInstances = pd.read_csv(pathToData+"extra_cacm_problem_info.csv")
dfInstances = dfInstances.rename(columns={"stations":"stationList", "name":"id"})
dfInstances.set_index("id", inplace=True, drop=True)

dfInterference = pd.read_table(pathToData+"Interference_Paired.csv", header=None, names=["interferenceType"])

#Manipulate so that the interferingIDs become a list
myColNames = ["interferenceType", "targetChannel", "interferingChannel", "targetID", "interferingIDs"]
dfInterference[myColNames] = dfInterference.interferenceType.str.split(",", n=4, expand=True)
dfInterference.interferingIDs = dfInterference.interferingIDs.str.split(",")

dfInterference[["targetChannel", "interferingChannel"]] = dfInterference[["targetChannel", "interferingChannel"]].apply(pd.to_numeric)
dfInterference = dfInterference.loc[(dfInterference["targetChannel"] >= 14) & (dfInterference["targetChannel"] <= 36)]
dfInterference = dfInterference.loc[(dfInterference["interferingChannel"] >= 14) & (dfInterference["interferingChannel"] <= 36)]

#Temporary!
#dfInstances = dfInstances[0:2]

avgDegrees = []
maxDegrees = []
diameters = []
sizes = []
sizesOfLargestCC = []
avgDegreesOfLargestCC = []
maxDegreesOfLargectCC = []
diametersOfLargestCC = []
numberOfCCs = []

for instance in dfInstances.itertuples():
	print(instance.Index)
	
	#This part creates the graph object
	V = dfInstances.loc[instance.Index,"stationList"][1:-1]
	V = V.split(", ")

	E = {}
	#We need to get the maximal list of interferingIDs to create the densest graph and then intersect with the 
	#stations that are actually in the instance.

	#The process of creating the edge set can probably e done better, if necessary
	counter = 0
	for v in V:
		counter += 1
		temp = dfInterference.loc[dfInterference["targetID"] == v]["interferingIDs"]
		interferingStations = set()
		for row in temp:
		    if row != None:
		        interferingStations.update(row)

		interferingStations = list(interferingStations)
		interferingStations = [station for station in interferingStations if station in V]
		E[v] = interferingStations

	adjacencyMatrix = pd.DataFrame(np.zeros((len(V),len(V)), dtype=int), index = V, columns = V)

	for v in V:
	    for e in E[v]:
	        adjacencyMatrix.at[v,e] = 1	

	E = []
	for u in V:
	    for v in V:
	        if adjacencyMatrix.loc[u,v] == 1:
	            E.append((u,v))

	G = nx.Graph()
	G.add_nodes_from(V)
	G.add_edges_from(E)

	nx.write_graphml(G, "/Users/haakonhr/Dropbox/TUM/Masteroppgave/FCC/graphml/"+instance.Index+".graphml")

	ccInfo = informationAboutLargestComponent(G)

	avgDegrees.append(avgDegree(G))
	maxDegrees.append(maxDegree(G))
	#diameters.append(nx.diameter(G)) can be disconnected so we need to take this into acocunt!
	sizes.append(len(G.nodes))
	numberOfCCs.append(nx.number_connected_components(G))
	sizesOfLargestCC.append(ccInfo[0])
	avgDegreesOfLargestCC.append(ccInfo[1])
	maxDegreesOfLargectCC.append(ccInfo[2])
	diametersOfLargestCC.append(ccInfo[3])

print(avgDegrees)
print(maxDegrees)
print(diameters)
print(sizes)

dfInstances["avgDegrees"] = avgDegrees
dfInstances["maxDegrees"] = maxDegrees
#dfInstances["diameters"] = diameters
dfInstances["size"] = sizes
dfInstances["numberOfCCs"] = numberOfCCs
dfInstances["sizesOfLargestCC"] = sizesOfLargestCC
dfInstances["avgDegreesOfLargestCC"] = avgDegreesOfLargestCC
dfInstances["maxDegreesOfLargectCC"] = maxDegreesOfLargectCC
dfInstances["diameterOfLargestCC"] = diametersOfLargestCC

#Density? An easy invariant, but can easily be computed from avgDegree
#Check for planarity and other boolean values

dfInstances.drop(["stationList","assignment"],axis=1).to_csv("/Users/haakonhr/Dropbox/TUM/Masteroppgave/FCC/merdata.csv")
print(dfInstances)
